import { describe } from "@b08/test-runner";
import { FileToParse } from "../../src";
import { createExportModel } from "../../src/createExportModel/createExportModel";
import { exportOptions } from "../../src/createExportModel/exportOptions";
import { ParsedClass, ParsedFunction, ParsedImport, ParsedModel } from "../../src/parseFile/types";
import { FunctionModel, TypesModel } from "../../src/types";
import { Synonym } from "../../src/types/synonym.type";
import { emptyExport, emptyParsed } from "./empty";

describe("createExportModel", it => {
  it("should process synonym chains", expect => {
    // arrange
    const srcFile1: FileToParse = { name: "file1", folder: "c:/folder/project/src", contents: "" };
    const index1: FileToParse = { name: "index", folder: "c:/folder/project/src/folder", contents: "" };
    const index2: FileToParse = { name: "index", folder: "c:/folder/project/src/folder/subfolder", contents: "" };
    const srcFile2: FileToParse = { name: "file2", folder: "c:/folder/project/src/folder/subfolder", contents: "" };

    const imp1: ParsedImport = { type: "Class2", alias: "Class2", importPath: "./folder" };

    const func1: ParsedFunction = {
      name: "func1", comments: [], async: false,
      parameters: [
        { parameterName: "p1", isOptional: false, typeName: "Class2" },
      ],
      returnType: "void"
    };

    const model1: ParsedModel = {
      ...emptyParsed,
      file: srcFile1,
      imports: [imp1],
      functions: [func1]
    };

    const syn1: Synonym = { path: "./subfolder" };
    const model2: ParsedModel = {
      ...emptyParsed,
      file: index1,
      synonyms: [syn1]
    };

    const syn2: Synonym = { path: "./file2" };
    const model3: ParsedModel = {
      ...emptyParsed,
      file: index2,
      synonyms: [syn2]
    };

    const cls2: ParsedClass = {
      name: "Class2", comments: [],
      fields: [], methods: [],
      extends: [], implements: [], constructor: { parameters: [], comments: [] }
    };

    const model4: ParsedModel = {
      ...emptyParsed,
      file: srcFile2,
      classes: [cls2]
    };

    // act
    const all = [model1, model2, model3, model4];
    const result = createExportModel(model1, all, exportOptions(all, {}));

    // assert
    const expFunc: FunctionModel = {
      id: {
        path: "c:/folder/project/src/file1",
        name: "func1"
      },
      comments: [],
      async: false,
      parameters: [
        {
          parameterName: "p1",
          isOptional: false,
          parameterType: {
            typeName: "Class2",
            genericArgs: [],
            type: {
              alias: "Class2",
              nodeModulesImport: null,
              id: {
                path: "c:/folder/project/src/folder/subfolder/file2",
                name: "Class2"
              }
            }
          }
        }

      ],
      returnType: { typeName: "void", genericArgs: [], type: { alias: "void", nodeModulesImport: null, id: null } }
    };

    const expected: TypesModel<FileToParse> = {
      ...emptyExport,
      file: srcFile1,
      functions: [expFunc]
    };

    expect.deepEqual(result, expected);
  });
});
