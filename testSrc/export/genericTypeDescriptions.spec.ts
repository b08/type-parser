import { describe } from "@b08/test-runner";
import { FileToParse } from "../../src";
import { createExportModel } from "../../src/createExportModel/createExportModel";
import { ParsedClass, ParsedFunction, ParsedModel } from "../../src/parseFile/types";
import { ClassModel, FunctionModel, ImportedType, TypesModel } from "../../src/types";
import { emptyExport, emptyParsed } from "./empty";

const cls1ImpType: ImportedType = {
  alias: "Class1", nodeModulesImport: null,
  id: { path: "c:/project/src/file1", name: "Class1" }
};
const cls2ImpType: ImportedType = {
  alias: "Class2", nodeModulesImport: null,
  id: { path: "c:/project/src/file1", name: "Class2" }
};
const promiseImpType: ImportedType = { alias: "Promise", nodeModulesImport: null, id: null };
const arrayImpType: ImportedType = { alias: "Array", nodeModulesImport: null, id: null };
const stringImpType: ImportedType = { alias: "string", nodeModulesImport: null, id: null };
const voidReturn = { typeName: "void", type: { alias: "void", id: null, nodeModulesImport: null }, genericArgs: [] };

describe("createExportModel", it => {
  it("should create simple generic types", expect => {
    // arrange
    const srcFile1: FileToParse = { name: "file1", folder: "c:/project/src", contents: "" };

    const func1: ParsedFunction = {
      name: "func1", comments: [], async: false,
      parameters: [
        { parameterName: "p1", isOptional: false, typeName: "Promise<Class1>" },
        { parameterName: "p2", isOptional: false, typeName: "Class1<string>" }
      ],
      returnType: "void"
    };

    const cls1: ParsedClass = {
      name: "Class1", comments: [],
      fields: [], methods: [],
      extends: [], implements: [], constructor: { parameters: [], comments: [] }
    };

    const model1: ParsedModel = {
      ...emptyParsed,
      file: srcFile1,
      functions: [func1],
      classes: [cls1]
    };

    // act
    const result = createExportModel(model1, [model1], null);

    // assert
    const expFunc: FunctionModel = {
      id: {
        path: "c:/project/src/file1",
        name: "func1"
      },
      comments: [],
      async: false,
      parameters: [{
        parameterName: "p1",
        isOptional: false,
        parameterType: {
          typeName: "Promise<Class1>",
          type: promiseImpType,
          genericArgs: [{
            typeName: "Class1",
            type: cls1ImpType,
            genericArgs: []
          }]
        }
      }, {
        parameterName: "p2",
        isOptional: false,
        parameterType: {
          typeName: "Class1<string>",
          type: cls1ImpType,
          genericArgs: [{
            typeName: "string",
            genericArgs: [],
            type: stringImpType,
          }]
        }
      }],
      returnType: voidReturn
    };

    const expCls: ClassModel = {
      id: {
        path: "c:/project/src/file1",
        name: "Class1"
      },
      comments: [],
      fields: [],
      methods: [],
      extends: [],
      implements: [],
      constructor: {
        parameters: [],
        comments: []
      }
    };

    const expected: TypesModel<FileToParse> = {
      ...emptyExport,
      file: srcFile1,
      functions: [expFunc],
      classes: [expCls]
    };

    expect.deepEqual(result, expected);
  });


  it("should create nested generic types", expect => {
    // arrange
    const srcFile1: FileToParse = { name: "file1", folder: "c:/project/src", contents: "" };

    const func1: ParsedFunction = {
      name: "func1", comments: [], async: false,
      parameters: [
        { parameterName: "p1", isOptional: false, typeName: "Promise<Class1<Class2>>" },
        { parameterName: "p2", isOptional: false, typeName: "Class1<Promise<Class2>>" }
      ],
      returnType: "void"
    };

    const cls1: ParsedClass = {
      name: "Class1", comments: [],
      fields: [], methods: [],
      extends: [], implements: [], constructor: { parameters: [], comments: [] }
    };

    const cls2: ParsedClass = {
      name: "Class2", comments: [],
      fields: [], methods: [],
      extends: [], implements: [], constructor: { parameters: [], comments: [] }
    };


    const model1: ParsedModel = {
      ...emptyParsed,
      file: srcFile1,
      functions: [func1],
      classes: [cls1, cls2]
    };

    // act
    const result = createExportModel(model1, [model1], null);

    // assert
    const expFunc: FunctionModel = {
      id: {
        path: "c:/project/src/file1",
        name: "func1"
      },
      comments: [],
      async: false,
      parameters: [{
        parameterName: "p1",
        isOptional: false,
        parameterType: {
          typeName: "Promise<Class1<Class2>>",
          type: promiseImpType,
          genericArgs: [{
            typeName: "Class1<Class2>",
            type: cls1ImpType,
            genericArgs: [{
              typeName: "Class2",
              type: cls2ImpType,
              genericArgs: []
            }],
          }]
        }
      }, {
        parameterName: "p2",
        isOptional: false,
        parameterType: {
          typeName: "Class1<Promise<Class2>>",
          type: cls1ImpType,
          genericArgs: [{
            typeName: "Promise<Class2>",
            type: promiseImpType,
            genericArgs: [{
              typeName: "Class2",
              type: cls2ImpType,
              genericArgs: []
            }],
          }]
        }
      }],
      returnType: voidReturn
    };

    const expCls1: ClassModel = {
      id: {
        path: "c:/project/src/file1",
        name: "Class1"
      },
      comments: [],
      fields: [],
      methods: [],
      extends: [],
      implements: [],
      constructor: {
        parameters: [],
        comments: []
      }
    };
    const expCls2: ClassModel = {
      id: {
        path: "c:/project/src/file1",
        name: "Class2"
      },
      comments: [],
      fields: [],
      methods: [],
      extends: [],
      implements: [],
      constructor: {
        parameters: [],
        comments: []
      }
    };

    const expected: TypesModel<FileToParse> = {
      ...emptyExport,
      file: srcFile1,
      functions: [expFunc],
      classes: [expCls1, expCls2]
    };

    expect.deepEqual(result, expected);
  });


  it("should create array type", expect => {
    // arrange
    const srcFile1: FileToParse = { name: "file1", folder: "c:/project/src", contents: "" };

    const func1: ParsedFunction = {
      name: "func1", comments: [], async: false,
      parameters: [
        { parameterName: "p1", isOptional: false, typeName: "Class1<Class2>[]" },
        { parameterName: "p2", isOptional: false, typeName: "Class1[]" }
      ],
      returnType: "void"
    };

    const cls1: ParsedClass = {
      name: "Class1", comments: [],
      fields: [], methods: [],
      extends: [], implements: [], constructor: { parameters: [], comments: [] }
    };

    const cls2: ParsedClass = {
      name: "Class2", comments: [],
      fields: [], methods: [],
      extends: [], implements: [], constructor: { parameters: [], comments: [] }
    };


    const model1: ParsedModel = {
      ...emptyParsed,
      file: srcFile1,
      functions: [func1],
      classes: [cls1, cls2]
    };

    // act
    const result = createExportModel(model1, [model1], null);

    // assert
    const expFunc: FunctionModel = {
      id: {
        path: "c:/project/src/file1",
        name: "func1"
      },
      comments: [],
      async: false,
      parameters: [{
        parameterName: "p1",
        isOptional: false,
        parameterType: {
          typeName: "Class1<Class2>[]",
          type: arrayImpType,
          genericArgs: [{
            typeName: "Class1<Class2>",
            type: cls1ImpType,
            genericArgs: [{
              typeName: "Class2",
              type: cls2ImpType,
              genericArgs: []
            }
            ],
          }]
        }
      }, {
        parameterName: "p2",
        isOptional: false,
        parameterType: {
          typeName: "Class1[]",
          type: arrayImpType,
          genericArgs: [{
            typeName: "Class1",
            type: cls1ImpType,
            genericArgs: []
          }]
        }
      }],
      returnType: voidReturn
    };

    const expCls1: ClassModel = {
      id: {
        path: "c:/project/src/file1",
        name: "Class1"
      },
      comments: [],
      fields: [],
      methods: [],
      extends: [],
      implements: [],
      constructor: {
        parameters: [],
        comments: []
      }
    };
    const expCls2: ClassModel = {
      id: {
        path: "c:/project/src/file1",
        name: "Class2"
      },
      comments: [],
      fields: [],
      methods: [],
      extends: [],
      implements: [],
      constructor: {
        parameters: [],
        comments: []
      }
    };

    const expected: TypesModel<FileToParse> = {
      ...emptyExport,
      file: srcFile1,
      functions: [expFunc],
      classes: [expCls1, expCls2]
    };

    expect.deepEqual(result, expected);
  });


  it("should create nested multi-generic type", expect => {
    // arrange
    const srcFile1: FileToParse = { name: "file1", folder: "c:/project/src", contents: "" };

    const func1: ParsedFunction = {
      name: "func1", comments: [], async: false,
      parameters: [
        { parameterName: "p1", isOptional: false, typeName: "Class1<Promise<Class2>, Class2<Promise<Class1>, Class1>>" },
      ],
      returnType: "void"
    };

    const cls1: ParsedClass = {
      name: "Class1", comments: [],
      fields: [], methods: [],
      extends: [], implements: [], constructor: { parameters: [], comments: [] }
    };

    const cls2: ParsedClass = {
      name: "Class2", comments: [],
      fields: [], methods: [],
      extends: [], implements: [], constructor: { parameters: [], comments: [] }
    };


    const model1: ParsedModel = {
      ...emptyParsed,
      file: srcFile1,
      functions: [func1],
      classes: [cls1, cls2]
    };

    // act
    const result = createExportModel(model1, [model1], null);

    // assert

    const expFunc: FunctionModel = {
      id: {
        path: "c:/project/src/file1",
        name: "func1"
      },
      comments: [],
      async: false,
      parameters: [{
        parameterName: "p1",
        isOptional: false,
        parameterType: {
          typeName: "Class1<Promise<Class2>, Class2<Promise<Class1>, Class1>>",
          type: cls1ImpType,
          genericArgs: [{
            typeName: "Promise<Class2>",
            type: promiseImpType,
            genericArgs: [{
              typeName: "Class2",
              type: cls2ImpType,
              genericArgs: []
            }],
          }, {
            typeName: "Class2<Promise<Class1>, Class1>",
            type: cls2ImpType,
            genericArgs: [{
              typeName: "Promise<Class1>",
              type: promiseImpType,
              genericArgs: [{
                typeName: "Class1",
                type: cls1ImpType,
                genericArgs: []
              }]
            }, {
              typeName: "Class1",
              type: cls1ImpType,
              genericArgs: []
            }]
          }]
        }
      }],
      returnType: { typeName: "void", type: { alias: "void", id: null, nodeModulesImport: null }, genericArgs: [] }
    };

    const expCls1: ClassModel = {
      id: {
        path: "c:/project/src/file1",
        name: "Class1"
      },
      comments: [],
      fields: [],
      methods: [],
      extends: [],
      implements: [],
      constructor: {
        parameters: [],
        comments: []
      }
    };
    const expCls2: ClassModel = {
      id: {
        path: "c:/project/src/file1",
        name: "Class2"
      },
      comments: [],
      fields: [],
      methods: [],
      extends: [],
      implements: [],
      constructor: {
        parameters: [],
        comments: []
      }
    };

    const expected: TypesModel<FileToParse> = {
      ...emptyExport,
      file: srcFile1,
      functions: [expFunc],
      classes: [expCls1, expCls2]
    };

    expect.deepEqual(result, expected);
  });
});
