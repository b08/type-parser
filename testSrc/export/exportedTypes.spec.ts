import { test } from "@b08/test-runner";
import { MethodModel, ClassModel, TypeModel, EnumModel, FieldModel, InterfaceModel, TypesModel } from "../../src/types";
import { FileToParse } from "../../src";
import { areEquivalent } from "@b08/array";
import { exportedTypes } from "../../src/functions/exportedTypes";

test("exported types should return type names", expect => {
  // arrange
  const expType: TypeModel = {
    id: { path: "c:/folder/f1", name: "type1" },
    comments: ["typeComment"]
  };

  const expEnum: EnumModel = {
    id: { path: "c:/folder/f1", name: "enum1" },
    comments: [],
    values: [{ valueName: "val1", comments: [] }]
  };
  const expClassMethod: MethodModel = {
    name: "func1", comments: [], async: false,
    parameters: [
      {
        parameterName: "p1",
        isOptional: false,
        parameterType: { typeName: "string", type: { alias: "string", id: null, nodeModulesImport: null }, genericArgs: [] }
      }],
    returnType: { typeName: "void", type: { alias: "void", id: null, nodeModulesImport: null }, genericArgs: [] }
  };
  const expClass: ClassModel = {
    id: { path: "c:/folder/f1", name: "class1" },
    fields: [],
    comments: [],
    constructor: { parameters: [], comments: [] },
    methods: [expClassMethod],
    extends: [],
    implements: []
  };

  const expIntField: FieldModel = {
    fieldName: "field1",
    isOptional: false,
    comments: [],
    fieldType: {
      typeName: "a",
      type: {
        alias: "a", nodeModulesImport: null, id: { path: "c:/folder/b", name: "a" },
      },
      genericArgs: []
    }
  };

  const expInterface: InterfaceModel = {
    id: { path: "c:/folder/f1", name: "iFace1" },
    comments: [],
    fields: [expIntField],
    methods: [],
    extends: []
  };

  const expConst: TypeModel = {
    id: { path: "c:/folder/f1", name: "constant" },
    comments: []
  };

  const model: TypesModel<FileToParse> = {
    file: null,
    types: [expType],
    enums: [expEnum],
    classes: [expClass],
    interfaces: [expInterface],
    functions: [],
    constants: [expConst]
  };

  const expected = ["type1", "enum1", "class1", "iFace1", "constant"];
  // act
  const result = exportedTypes(model);

  // arrange
  expect.true(areEquivalent(result, expected));
});
