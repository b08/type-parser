export const emptyExport = {
  types: [],
  enums: [],
  classes: [],
  interfaces: [],
  functions: [],
  constants: []
};

export const emptyParsed = {
  ...emptyExport,
  imports: [],
  synonyms: []
};
