import { describe } from "@b08/test-runner";
import { FileToParse } from "../../src";
import { createExportModel } from "../../src/createExportModel/createExportModel";
import { exportOptions } from "../../src/createExportModel/exportOptions";
import { ParsedClass, ParsedEnum, ParsedField, ParsedFunction, ParsedImport, ParsedInterface, ParsedModel, ParsedType } from "../../src/parseFile/types";
import { ClassModel, EnumModel, FieldModel, InterfaceModel, MethodModel, TypeModel, TypesModel } from "../../src/types";

describe("createExportModel", it => {
  it("should create export model from parsed", expect => {
    // arrange
    const file: FileToParse = { name: "f1", folder: "c:/folder/subFolder", contents: "" };

    const imp: ParsedImport = { type: "a", alias: "a", importPath: "./b" };
    const type: ParsedType = { name: "type1", comments: ["typeComment"] };
    const en: ParsedEnum = { name: "enum1", comments: [], values: [{ valueName: "val1", comments: [] }] };
    const clsMethod: ParsedFunction = {
      name: "func1", comments: [], async: false,
      parameters: [{ parameterName: "p1", isOptional: false, typeName: "string" }], returnType: "void"
    };
    const cls: ParsedClass = {
      name: "class1", comments: [], fields: [],
      methods: [clsMethod], constructor: { parameters: [], comments: [] }, extends: [], implements: []
    };
    const infField: ParsedField = { fieldName: "field1", isOptional: false, comments: [], typeName: "a" };
    const inf: ParsedInterface = { name: "iFace1", comments: [], fields: [infField], methods: [], extends: [] };
    const constant: ParsedType = { name: "constant", comments: [] };

    const parsed: ParsedModel = {
      file,
      imports: [imp],
      types: [type],
      enums: [en],
      classes: [cls],
      interfaces: [inf],
      functions: [],
      constants: [constant],
      synonyms: []
    };

    const expType: TypeModel = {
      id: { path: "c:/folder/subFolder/f1", name: "type1" },
      comments: ["typeComment"]
    };

    const expEnum: EnumModel = {
      id: { path: "c:/folder/subFolder/f1", name: "enum1" },
      comments: [],
      values: [{ valueName: "val1", comments: [] }]
    };
    const expClassMethod: MethodModel = {
      name: "func1", comments: [], async: false,
      parameters: [
        {
          parameterName: "p1",
          isOptional: false,
          parameterType: { typeName: "string", type: { alias: "string", id: null, nodeModulesImport: null }, genericArgs: [] }
        }],
      returnType: { typeName: "void", type: { alias: "void", id: null, nodeModulesImport: null }, genericArgs: [] }
    };
    const expClass: ClassModel = {
      id: { path: "c:/folder/subFolder/f1", name: "class1" },
      fields: [],
      comments: [],
      constructor: { parameters: [], comments: [] },
      methods: [expClassMethod],
      extends: [],
      implements: []
    };

    const expIntField: FieldModel = {
      fieldName: "field1",
      isOptional: false,
      comments: [],
      fieldType: {
        typeName: "a",
        type: {
          alias: "a",
          id: { path: "c:/folder/subFolder/b", name: "a" },
          nodeModulesImport: null
        },
        genericArgs: []
      }
    };

    const expInterface: InterfaceModel = {
      id: { path: "c:/folder/subFolder/f1", name: "iFace1" },
      comments: [],
      fields: [expIntField],
      methods: [],
      extends: []
    };

    const expConst: TypeModel = {
      id: { path: "c:/folder/subFolder/f1", name: "constant" },
      comments: []
    };

    const expected: TypesModel<FileToParse> = {
      file,
      types: [expType],
      enums: [expEnum],
      classes: [expClass],
      interfaces: [expInterface],
      functions: [],
      constants: [expConst]
    };

    // act
    const result = createExportModel(parsed, [parsed], exportOptions([parsed], {}));

    // assert
    expect.deepEqual(result, expected);
  });

});
