import { describe } from "@b08/test-runner";
import { parseFile } from "../../src/parseFile/parseFile";
import { ParsedFunction } from "../../src/parseFile/types";
import { file } from "./file";

describe("parse functions", it => {
  it("should parse a function", expect => {
    // arrange

    const content = `
    export function someFunction(par: string): number {
      console.log("hello");
    }
`;
    const expected: ParsedFunction[] = [{
      comments: [],
      async: false,
      name: "someFunction",
      returnType: "number",
      parameters: [{ parameterName: "par", isOptional: false, typeName: "string" }]
    }];

    // act
    const result = parseFile(file(content));

    // assert
    expect.deepEqual(result.functions, expected);
  });

  it("should parse async function", expect => {
    // arrange

    const content = `
    export async function someFunction(par: string): Promise<number> {
      console.log("hello");
    }
`;
    const expected: ParsedFunction[] = [{
      comments: [],
      async: true,
      name: "someFunction",
      returnType: "Promise<number>",
      parameters: [{ parameterName: "par", isOptional: false, typeName: "string" }]
    }];

    // act
    const result = parseFile(file(content));

    // assert
    expect.deepEqual(result.functions, expected);
  });

  it("should parse one line function", expect => {
    // arrange

    const content = `
    export function someFunction(par: string): number { console.log("hello"); }
`;
    const expected: ParsedFunction[] = [{
      comments: [],
      async: false,
      name: "someFunction",
      returnType: "number",
      parameters: [{ parameterName: "par", isOptional: false, typeName: "string" }]
    }];

    // act
    const result = parseFile(file(content));

    // assert
    expect.deepEqual(result.functions, expected);
  });

  it("should parse function with comments", expect => {
    // arrange

    const content = `
    // very good function
    export function someFunction(par: string): number {
      console.log("hello");
    }
`;
    const expected: ParsedFunction[] = [{
      comments: ["very good function"],
      name: "someFunction",
      async: false,
      returnType: "number",
      parameters: [{ parameterName: "par", isOptional: false, typeName: "string" }]
    }];

    // act
    const result = parseFile(file(content));

    // assert
    expect.deepEqual(result.functions, expected);
  });

  it("should parse function with 2 comments", expect => {
    // arrange

    const content = `
    // comment2
    // very good function
    export function someFunction(par: string): number {
      console.log("hello");
    }
`;
    const expected: ParsedFunction[] = [{
      comments: ["comment2", "very good function"],
      name: "someFunction",
      async: false,
      returnType: "number",
      parameters: [{ parameterName: "par", isOptional: false, typeName: "string" }]
    }];

    // act
    const result = parseFile(file(content));

    // assert
    expect.deepEqual(result.functions, expected);
  });

  it("should parse function with lots of comments", expect => {
    // arrange

    const content = `
    /* comment1 | comment2
    */
    // comment3
    // comment4 | comment5
    export function someFunction(par: string): number {
      console.log("hello");
    }
`;
    const expected: ParsedFunction[] = [{
      comments: ["comment1", "comment2", "comment3", "comment4", "comment5"],
      name: "someFunction",
      async: false,
      returnType: "number",
      parameters: [{ parameterName: "par", isOptional: false, typeName: "string" }]
    }];

    // act
    const result = parseFile(file(content));

    // assert
    expect.deepEqual(result.functions, expected);
  });

  it("should only include comments if only spaces between function and comments", expect => {
    // arrange

    const content = `
    // comments is not for a function
    const a = 1;
    export  function someFunction(par: string): number {
      console.log("hello");
    }
`;
    const expected: ParsedFunction[] = [{
      comments: [],
      name: "someFunction",
      returnType: "number",
      async: false,
      parameters: [{ parameterName: "par", isOptional: false, typeName: "string" }]
    }];

    // act
    const result = parseFile(file(content));

    // assert
    expect.deepEqual(result.functions, expected);
  });

  it("should not include commented function", expect => {
    // arrange

    const content = `
   // export function someFunction(par: string): number {
   /* export function someFunction(par: string): number {
      console.log("hello");
    }*/
`;
    const expected: ParsedFunction[] = [];

    // act
    const result = parseFile(file(content));

    // assert
    expect.deepEqual(result.functions, expected);
  });

  it("should parse three functions", expect => {
    // arrange

    const content = `

    export function someFunction(par: string): number {
      console.log("hello");
    }

    // second
    export function someFunction2(par: string): number {
      console.log("hello");
    }

    export function someFunction3(par: string): number {
      console.log("hello");
    }
`;
    const expected: ParsedFunction[] = [{
      comments: [],
      name: "someFunction",
      returnType: "number",
      async: false,
      parameters: [{ parameterName: "par", isOptional: false, typeName: "string" }]
    },
    {
      comments: ["second"],
      async: false,
      name: "someFunction2",
      returnType: "number",
      parameters: [{ parameterName: "par", isOptional: false, typeName: "string" }]
    },
    {
      comments: [],
      name: "someFunction3",
      async: false,
      returnType: "number",
      parameters: [{ parameterName: "par", isOptional: false, typeName: "string" }]
    }];

    // act
    const result = parseFile(file(content));

    // assert
    expect.deepEqual(result.functions, expected);
  });

  it("should parse function with several parameters", expect => {
    // arrange

    const content = `

    export function someFunction(par1: string, par2: number): number {
      console.log("hello");
    }

`;
    const expected: ParsedFunction[] = [{
      comments: [],
      name: "someFunction",
      async: false,
      returnType: "number",
      parameters: [
        { parameterName: "par1", isOptional: false, typeName: "string" },
        { parameterName: "par2", isOptional: false, typeName: "number" }
      ]
    }];

    // act
    const result = parseFile(file(content));

    // assert
    expect.deepEqual(result.functions, expected);
  });

  it("should parse function with generic parameters", expect => {
    // arrange

    const content = `

    export function someFunction(par1: GenType<string, number>, par2: number): number {
      console.log("hello");
    }

`;
    const expected: ParsedFunction[] = [{
      comments: [],
      name: "someFunction",
      returnType: "number",
      async: false,
      parameters: [
        { parameterName: "par1", isOptional: false, typeName: "GenType<string, number>" },
        { parameterName: "par2", isOptional: false, typeName: "number" }
      ]
    }];

    // act
    const result = parseFile(file(content));

    // assert
    expect.deepEqual(result.functions, expected);
  });

  it("should parse function with optional parameter", expect => {
    // arrange

    const content = `

    export function someFunction(par1: string, par2?: number): number {
      console.log("hello");
    }

`;
    const expected: ParsedFunction[] = [{
      comments: [],
      name: "someFunction",
      async: false,
      returnType: "number",
      parameters: [
        { parameterName: "par1", isOptional: false, typeName: "string" },
        { parameterName: "par2", isOptional: true, typeName: "number" }
      ]
    }];

    // act
    const result = parseFile(file(content));

    // assert
    expect.deepEqual(result.functions, expected);
  });

  it("should not break on 'less' sign in functions", expect => {
    // arrange

    const content = `

    export function someFunction(par: string): boolean {
      return par.length < 10;
    }

    // second
    export function someFunction2(par: string): number {
      console.log("hello");
    }

    export function someFunction3(par: string): number {
      console.log("hello");
    }
`;
    const expected: ParsedFunction[] = [{
      comments: [],
      name: "someFunction",
      returnType: "boolean",
      async: false,
      parameters: [{ parameterName: "par", isOptional: false, typeName: "string" }]
    },
    {
      comments: ["second"],
      async: false,
      name: "someFunction2",
      returnType: "number",
      parameters: [{ parameterName: "par", isOptional: false, typeName: "string" }]
    },
    {
      comments: [],
      name: "someFunction3",
      async: false,
      returnType: "number",
      parameters: [{ parameterName: "par", isOptional: false, typeName: "string" }]
    }];

    // act
    const result = parseFile(file(content));

    // assert
    expect.deepEqual(result.functions, expected);
  });
});
