import { describe } from "@b08/test-runner";
import { parseFile } from "../../src/parseFile/parseFile";
import { ParsedSynonym } from "../../src/parseFile/types/parsedSynonym.type";
import { file } from "./file";

describe("parse synonyms", it => {
  it("should parse synonym", expect => {
    // arrange

    const content = `
    export * from "./file";
`;
    const expected: ParsedSynonym[] = [{ path: "./file" }];

    // act
    const result = parseFile(file(content));

    // assert
    expect.deepEqual(result.synonyms, expected);
  });

  it("should not change long paths", expect => {
    // arrange

    const content = `
    export * from "./folder/file";
`;
    const expected: ParsedSynonym[] = [{ path: "./folder/file" }];

    // act
    const result = parseFile(file(content));

    // assert
    expect.deepEqual(result.synonyms, expected);
  });

});
