import { describe } from "@b08/test-runner";
import { parseFile } from "../../src/parseFile/parseFile";
import { ParsedClass } from "../../src/parseFile/types";
import { ParsedConstructor } from "../../src/parseFile/types/parsedConstructor.type";
import { file } from "./file";

const emtyCtor: ParsedConstructor = {
  comments: [],
  parameters: []
};

describe("parse class", it => {
  it("should parse empty class", expect => {
    // arrange

    const content = `
    export class SomeClass {

    }
`;

    const expected: ParsedClass[] = [{
      comments: [],
      name: "SomeClass",
      extends: [],
      implements: [],
      constructor: emtyCtor,
      fields: [],
      methods: []
    }];

    // act
    const result = parseFile(file(content));

    // assert
    expect.deepEqual(result.classes, expected);
  });

  it("should parse empty one-line class", expect => {
    // arrange

    const content = `
    export class SomeClass { }
`;
    const expected: ParsedClass[] = [{
      comments: [],
      name: "SomeClass",
      extends: [],
      implements: [],
      constructor: emtyCtor,
      fields: [],
      methods: []
    }];

    // act
    const result = parseFile(file(content));

    // assert
    expect.deepEqual(result.classes, expected);
  });

  it("should parse class with extends", expect => {
    // arrange

    const content = `
    export class SomeClass extends OtherClass {
    }
`;
    const expected: ParsedClass[] = [{
      comments: [],
      name: "SomeClass",
      extends: ["OtherClass"],
      implements: [],
      constructor: emtyCtor,
      fields: [],
      methods: []
    }];

    // act
    const result = parseFile(file(content));

    // assert
    expect.deepEqual(result.classes, expected);
  });

  it("should parse class with implements", expect => {
    // arrange

    const content = `
    export class SomeClass implements SomeInterface, OtherInterface {
    }
`;
    const expected: ParsedClass[] = [{
      comments: [],
      name: "SomeClass",
      extends: [],
      implements: ["SomeInterface", "OtherInterface"],
      constructor: emtyCtor,
      fields: [],
      methods: []
    }];

    // act
    const result = parseFile(file(content));

    // assert
    expect.deepEqual(result.classes, expected);
  });

  it("should parse class with extends and implements", expect => {
    // arrange

    const content = `
    export class SomeClass extends OtherClass implements SomeInterface, OtherInterface {
    }
`;
    const expected: ParsedClass[] = [{
      comments: [],
      name: "SomeClass",
      extends: ["OtherClass"],
      implements: ["SomeInterface", "OtherInterface"],
      constructor: emtyCtor,
      fields: [],
      methods: []
    }];

    // act
    const result = parseFile(file(content));

    // assert
    expect.deepEqual(result.classes, expected);
  });

  it("should parse class with one parameter constructor", expect => {
    // arrange

    const content = `
    export class SomeClass {
      constructor(a: number){
      }
    }
`;
    const expected: ParsedClass[] = [{
      comments: [],
      name: "SomeClass",
      extends: [],
      implements: [],
      constructor: {
        comments: [],
        parameters: [{
          parameterName: "a",
          isOptional: false,
          typeName: "number"
        }],
      },
      fields: [],
      methods: []
    }];

    // act
    const result = parseFile(file(content));

    // assert
    expect.deepEqual(result.classes, expected);
  });

  it("should parse class with constructor and public parameter", expect => {
    // arrange

    const content = `
    export class SomeClass {
      constructor(public a: number){
      }
    }
`;
    const expected: ParsedClass[] = [{
      comments: [],
      name: "SomeClass",
      extends: [],
      implements: [],
      constructor: {
        comments: [],
        parameters: [{
          parameterName: "a",
          isOptional: false,
          typeName: "number"
        }]
      },
      fields: [{
        fieldName: "a",
        typeName: "number",
        isOptional: false,
        comments: []
      }],
      methods: []
    }];

    // act
    const result = parseFile(file(content));

    // assert
    expect.deepEqual(result.classes, expected);
  });

  it("should parse class with constructor and public/private parameters", expect => {
    // arrange

    const content = `
    export class SomeClass {
      constructor(private b: string, public a: number){
      }
    }
`;
    const expected: ParsedClass[] = [{
      comments: [],
      name: "SomeClass",
      extends: [],
      implements: [],
      constructor: {
        comments: [],
        parameters: [{
          parameterName: "b",
          isOptional: false,
          typeName: "string"
        }, {
          parameterName: "a",
          isOptional: false,
          typeName: "number"
        }]
      },
      fields: [{
        fieldName: "a",
        isOptional: false,
        typeName: "number",
        comments: []
      }],
      methods: []
    }];

    // act
    const result = parseFile(file(content));

    // assert
    expect.deepEqual(result.classes, expected);
  });

  it("should parse class with constructor and public optional parameters", expect => {
    // arrange

    const content = `
    export class SomeClass {
      constructor(public a?: number){
      }
    }
`;
    const expected: ParsedClass[] = [{
      comments: [],
      name: "SomeClass",
      extends: [],
      implements: [],
      constructor: {
        comments: [],
        parameters: [{
          parameterName: "a",
          isOptional: true,
          typeName: "number"
        }]
      },
      fields: [{
        fieldName: "a",
        isOptional: false,
        typeName: "number",
        comments: []
      }],
      methods: []
    }];

    // act
    const result = parseFile(file(content));

    // assert
    expect.deepEqual(result.classes, expected);
  });


  it("should parse class with method and then constructor ", expect => {
    // arrange

    const content = `
    export class SomeClass {
      public m1(): void {

      }
      constructor(private b: string){
      }
    }
`;
    const expected: ParsedClass[] = [{
      comments: [],
      name: "SomeClass",
      extends: [],
      implements: [],
      constructor: {
        comments: [],
        parameters: [{
          parameterName: "b",
          isOptional: false,
          typeName: "string"
        }]
      },
      fields: [],
      methods: [{
        name: "m1",
        async: false,
        parameters: [],
        returnType: "void",
        comments: []
      }]
    }];

    // act
    const result = parseFile(file(content));

    // assert
    expect.deepEqual(result.classes, expected);
  });

  it("should parse class with async method ", expect => {
    // arrange

    const content = `
    export class SomeClass {
      public async m1(): Promise<void> {
      }
    }
`;
    const expected: ParsedClass[] = [{
      comments: [],
      name: "SomeClass",
      extends: [],
      implements: [],
      constructor: emtyCtor,
      fields: [],
      methods: [{
        name: "m1",
        async: true,
        parameters: [],
        returnType: "Promise<void>",
        comments: []
      }]
    }];

    // act
    const result = parseFile(file(content));

    // assert
    expect.deepEqual(result.classes, expected);
  });

  it("should parse class with empty constructor, field and commented method ", expect => {
    // arrange

    const content = `
    export class SomeClass {
      constructor() {
      }

      public f1: MyType<number, MyOtherType> = 10;

      // method comments
      public m1(): void {
      }
    }
`;
    const expected: ParsedClass[] = [{
      comments: [],
      name: "SomeClass",
      extends: [],
      implements: [],
      constructor: {
        comments: [],
        parameters: []
      },
      fields: [{
        fieldName: "f1",
        isOptional: false,
        typeName: "MyType<number, MyOtherType>",
        comments: []
      }],
      methods: [{
        name: "m1",
        async: false,
        parameters: [],
        returnType: "void",
        comments: ["method comments"]
      }]
    }];

    // act
    const result = parseFile(file(content));

    // assert
    expect.deepEqual(result.classes, expected);
  });

  it("should parse class method returning anonymous object ", expect => {
    // arrange

    const content = `
    export class SomeClass {
      public m1(): {a: string} {
      }
    }
`;
    const expected: ParsedClass[] = [{
      comments: [],
      name: "SomeClass",
      extends: [],
      implements: [],
      constructor: emtyCtor,
      fields: [],
      methods: [{
        name: "m1",
        async: false,
        parameters: [],
        returnType: "{a: string}",
        comments: []
      }]
    }];

    // act
    const result = parseFile(file(content));

    // assert
    expect.deepEqual(result.classes, expected);
  });

  it("should parse class method returning arrow function ", expect => {
    // arrange

    const content = `
    export class SomeClass {
      public m1(): (a: string) => string {
      }
    }
`;
    const expected: ParsedClass[] = [{
      comments: [],
      name: "SomeClass",
      extends: [],
      implements: [],
      constructor: emtyCtor,
      fields: [],
      methods: [{
        name: "m1",
        async: false,
        parameters: [],
        returnType: "(a: string) => string",
        comments: []
      }]
    }];

    // act
    const result = parseFile(file(content));

    // assert
    expect.deepEqual(result.classes, expected);
  });


  it("should parse class and ignore private fields in it ", expect => {
    // arrange

    const content = `
    export class SomeClass {
      private m1(): {a: string} {
      }

      private f1: string;

      public f2: number;
    }
`;
    const expected: ParsedClass[] = [{
      comments: [],
      name: "SomeClass",
      extends: [],
      implements: [],
      constructor: emtyCtor,
      fields: [{
        fieldName: "f2",
        isOptional: false,
        typeName: "number",
        comments: []
      }],
      methods: []
    }];

    // act
    const result = parseFile(file(content));

    // assert
    expect.deepEqual(result.classes, expected);
  });
});
