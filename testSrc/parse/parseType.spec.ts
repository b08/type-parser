import { parseFile } from "../../src/parseFile/parseFile";
import { describe } from "@b08/test-runner";
import { ParsedType } from "../../src/parseFile/types";
import { file } from "./file";

describe("parse type", it => {
  it("should parse union type", expect => {
    // arrange

    const content = `
    export type t1 = "a" | "b";
`;
    const expected: ParsedType[] = [{
      comments: [],
      name: "t1"
    }];

    // act
    const result = parseFile(file(content));

    // assert
    expect.deepEqual(result.types, expected);
  });

  it("should parse function type, interface type and union", expect => {
    // arrange

    const content = `
    export type t1 = function(): void { };

    export type t2 = { a: number };

    export type t3 = "a" | "b";
`;
    const expected: ParsedType[] = [{
      comments: [],
      name: "t1"
    }, {
      comments: [],
      name: "t2"
    }, {
      comments: [],
      name: "t3"
    }];

    // act
    const result = parseFile(file(content));

    // assert
    expect.deepEqual(result.types, expected);
  });

  it("should parse constant and type with comments", expect => {
    // arrange

    const content = `
    // type comments
    export type t1 = function(): void { };

    export constant c2 = { a: number };
`;
    const expectedTypes: ParsedType[] = [{
      comments: ["type comments"],
      name: "t1"
    }];

    const expectedConstants: ParsedType[] = [{
      comments: [],
      name: "c2"
    }];
    // act
    const result = parseFile(file(content));

    // assert
    expect.deepEqual(result.types, expectedTypes);
    expect.deepEqual(result.constants, expectedConstants);
  });
});
