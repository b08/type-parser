import { describe } from "@b08/test-runner";
import { parseFile } from "../../src/parseFile/parseFile";
import { ParsedFunction, ParsedInterface } from "../../src/parseFile/types";
import { file } from "./file";

describe("parse interface", it => {
  it("should parse empty interface", expect => {
    // arrange

    const content = `
    export interface SomeInterface {

    }
`;
    const expected: ParsedInterface[] = [{
      comments: [],
      name: "SomeInterface",
      extends: [],
      fields: [],
      methods: []
    }];

    // act
    const result = parseFile(file(content));

    // assert
    expect.deepEqual(result.interfaces, expected);
  });

  it("should parse empty one-line interface", expect => {
    // arrange

    const content = `
    export interface SomeInterface { }
`;
    const expected: ParsedInterface[] = [{
      comments: [],
      name: "SomeInterface",
      extends: [],
      fields: [],
      methods: []
    }];

    // act
    const result = parseFile(file(content));

    // assert
    expect.deepEqual(result.interfaces, expected);
  });

  it("should parse empty interface with extends", expect => {
    // arrange

    const content = `
    export interface SomeInterface extends OtherInterface, ThirdInterface {
    }
`;
    const expected: ParsedInterface[] = [{
      comments: [],
      name: "SomeInterface",
      extends: ["OtherInterface", "ThirdInterface"],
      fields: [],
      methods: []
    }];

    // act
    const result = parseFile(file(content));

    // assert
    expect.deepEqual(result.interfaces, expected);
  });

  it("should parse interface with a field", expect => {
    // arrange

    const content = `
    export interface SomeInterface {
    f1: number;
    }
`;
    const expected: ParsedInterface[] = [{
      comments: [],
      name: "SomeInterface",
      extends: [],
      fields: [{
        fieldName: "f1",
        isOptional: false,
        typeName: "number",
        comments: []
      }],
      methods: []
    }];

    // act
    const result = parseFile(file(content));

    // assert
    expect.deepEqual(result.interfaces, expected);
  });

  it("should parse interface with a method", expect => {
    // arrange

    const content = `
    export interface SomeInterface {
    f1: (a: number) => string;
    }
`;
    const expected: ParsedInterface[] = [{
      comments: [],
      name: "SomeInterface",
      extends: [],
      fields: [],
      methods: [{
        comments: [],
        returnType: "string",
        async: false,
        name: "f1",
        parameters: [{ parameterName: "a", isOptional: false, typeName: "number" }]
      }]
    }];

    // act
    const result = parseFile(file(content));

    // assert
    expect.deepEqual(result.interfaces, expected);
  });

  it("should parse interface with direct method", expect => {
    // arrange

    const content = `
    export interface SomeInterface {
    f1(a: number): string;
    }
`;
    const expected: ParsedInterface[] = [{
      comments: [],
      name: "SomeInterface",
      extends: [],
      fields: [],
      methods: [{
        comments: [],
        returnType: "string",
        async: false,
        name: "f1",
        parameters: [{ parameterName: "a", isOptional: false, typeName: "number" }]
      }]
    }];

    // act
    const result = parseFile(file(content));

    // assert
    expect.deepEqual(result.interfaces, expected);
  });

  it("should parse interface with a method and a field", expect => {
    // arrange

    const content = `
    export interface SomeInterface {
      f1: (a: number) => string;
      f2: number;
    }
`;
    const expected: ParsedInterface[] = [{
      comments: [],
      name: "SomeInterface",
      extends: [],
      fields: [{
        fieldName: "f2",
        isOptional: false,
        typeName: "number",
        comments: []
      }],
      methods: [{
        comments: [],
        returnType: "string",
        async: false,
        name: "f1",
        parameters: [{ parameterName: "a", isOptional: false, typeName: "number" }]
      }]
    }];

    // act
    const result = parseFile(file(content));

    // assert
    expect.deepEqual(result.interfaces, expected);
  });

  it("should parse interface with a method and commented field", expect => {
    // arrange

    const content = `
    export interface SomeInterface {
      f1: (a: number) => string;
      // field comments
      f2: number;
    }
`;
    const expected: ParsedInterface[] = [{
      comments: [],
      name: "SomeInterface",
      extends: [],
      fields: [{
        fieldName: "f2",
        isOptional: false,
        typeName: "number",
        comments: ["field comments"]
      }],
      methods: [{
        comments: [],
        returnType: "string",
        async: false,
        name: "f1",
        parameters: [{ parameterName: "a", isOptional: false, typeName: "number" }]
      }]
    }];

    // act
    const result = parseFile(file(content));

    // assert
    expect.deepEqual(result.interfaces, expected);
  });

  it("should parse interface with commented method and a field", expect => {
    // arrange

    const content = `
    export interface SomeInterface {
      // method comments
      f1: (a: number) => string;

      f2: number;
    }
`;
    const expected: ParsedInterface[] = [{
      comments: [],
      name: "SomeInterface",
      extends: [],
      fields: [{
        fieldName: "f2",
        isOptional: false,
        typeName: "number",
        comments: []
      }],
      methods: [{
        comments: ["method comments"],
        async: false,
        returnType: "string",
        name: "f1",
        parameters: [{ parameterName: "a", isOptional: false, typeName: "number" }]
      }]
    }];

    // act
    const result = parseFile(file(content));

    // assert
    expect.deepEqual(result.interfaces, expected);
  });

  it("should parse interface with two fields", expect => {
    // arrange

    const content = `
    export interface SomeInterface {
      f1: MyType;
      f2: OtherType;
    }
`;
    const expected: ParsedInterface[] = [{
      comments: [],
      name: "SomeInterface",
      extends: [],
      fields: [{
        fieldName: "f1",
        isOptional: false,
        typeName: "MyType",
        comments: []
      }, {
        fieldName: "f2",
        isOptional: false,
        typeName: "OtherType",
        comments: []
      }],
      methods: []
    }];

    // act
    const result = parseFile(file(content));

    // assert
    expect.deepEqual(result.interfaces, expected);
  });

  it("should parse interface with optional field", expect => {
    // arrange

    const content = `
    export interface SomeInterface {
      f1: MyType;
      f2?: OtherType;
    }
`;
    const expected: ParsedInterface[] = [{
      comments: [],
      name: "SomeInterface",
      extends: [],
      fields: [{
        fieldName: "f1",
        isOptional: false,
        typeName: "MyType",
        comments: []
      }, {
        fieldName: "f2",
        isOptional: true,
        typeName: "OtherType",
        comments: []
      }],
      methods: []
    }];

    // act
    const result = parseFile(file(content));

    // assert
    expect.deepEqual(result.interfaces, expected);
  });

  it("should parse interface with two methods", expect => {
    // arrange

    const content = `
    export interface SomeInterface {
      f1: () => MyType;
      f2: (a: Type) =>OtherType;
    }
`;
    const expected: ParsedInterface[] = [{
      comments: [],
      name: "SomeInterface",
      extends: [],
      fields: [],
      methods: [{
        name: "f1",
        async: false,
        parameters: [],
        comments: [],
        returnType: "MyType"
      }, {
        name: "f2",
        async: false,
        parameters: [{ parameterName: "a", isOptional: false, typeName: "Type" }],
        comments: [],
        returnType: "OtherType"
      }]
    }];

    // act
    const result = parseFile(file(content));

    // assert
    expect.deepEqual(result.interfaces, expected);
  });

  it("should parse interface and a free function", expect => {
    // arrange

    const content = `
    export interface SomeInterface {
    }

    export function myFunction(): void { }
`;
    const expectedInterfaces: ParsedInterface[] = [{
      comments: [],
      name: "SomeInterface",
      extends: [],
      fields: [],
      methods: []
    }];

    const expectedFunctions: ParsedFunction[] = [{
      name: "myFunction",
      async: false,
      comments: [],
      parameters: [],
      returnType: "void"
    }];

    // act
    const result = parseFile(file(content));

    // assert
    expect.deepEqual(result.interfaces, expectedInterfaces);
    expect.deepEqual(result.functions, expectedFunctions);
  });
});
