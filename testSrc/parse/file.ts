import { FileToParse } from "../../src";

export function file(contents: string): FileToParse { return { contents, folder: null, name: null }; }
