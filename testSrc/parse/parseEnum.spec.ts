import { parseFile } from "../../src/parseFile/parseFile";
import { describe } from "@b08/test-runner";
import { ParsedEnum } from "../../src/parseFile/types";
import { file } from "./file";

describe("parse enum", it => {
  it("should parse empty enum", expect => {
    // arrange

    const content = `
    export enum MyEnum {}
`;
    const expected: ParsedEnum[] = [{
      comments: [],
      name: "MyEnum",
      values: []
    }];

    // act
    const result = parseFile(file(content));

    // assert
    expect.deepEqual(result.enums, expected);
  });

  it("should parse enum with values and comments", expect => {
    // arrange

    const content = `
    // enum comments
    export enum MyEnum {
      a = 1,
      // value comments
      b = "v",
      c
    }
`;
    const expected: ParsedEnum[] = [{
      comments: ["enum comments"],
      name: "MyEnum",
      values: [{
        valueName: "a",
        comments: []
      }, {
        valueName: "b",
        comments: ["value comments"]
      }, {
        valueName: "c",
        comments: []
      }]
    }];

    // act
    const result = parseFile(file(content));

    // assert
    expect.deepEqual(result.enums, expected);
  });

});
