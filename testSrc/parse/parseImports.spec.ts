import { describe, IExpect } from "@b08/test-runner";
import { parseFile } from "../../src/parseFile/parseFile";
import { ParsedImport } from "../../src/parseFile/types";
import { file } from "./file";

describe("parse imports", it => {
  it("should parse singular imports", expect => {
    // arrange

    const content = `
    import { Type1 } from '.';
    import { Type2 } from './';
`;
    const expected: ParsedImport[] = [
      { type: "Type1", alias: "Type1", importPath: "." },
      { type: "Type2", alias: "Type2", importPath: "./" }
    ];

    // act
    const result = parseFile(file(content));

    // assert
    checkImports(expect, result.imports, expected);
  });

  it("should ignore commented line", expect => {
    // arrange

    const content = `
    // import { Type1 } from '.';
    import { Type2 } from './';
`;
    const expected: ParsedImport[] = [
      { type: "Type2", alias: "Type2", importPath: "./" }
    ];

    // act
    const result = parseFile(file(content));

    // assert
    checkImports(expect, result.imports, expected);
  });

  it("should ignore star comments", expect => {
    // arrange

    const content = `
    /* import { Type1 } from '.';
    */ import { Type2 } from './';
`;
    const expected: ParsedImport[] = [
      { type: "Type2", alias: "Type2", importPath: "./" }
    ];

    // act
    const result = parseFile(file(content));

    // assert
    checkImports(expect, result.imports, expected);
  });

  it("should parse multi imports", expect => {
    // arrange
    const content = `
    import { Type1, Type2 } from '.';
`;
    const expected: ParsedImport[] = [
      { type: "Type1", alias: "Type1", importPath: "." },
      { type: "Type2", alias: "Type2", importPath: "." }
    ];

    // act
    const result = parseFile(file(content));

    // assert
    checkImports(expect, result.imports, expected);
  });

  it("should parse aliased imports", expect => {
    // arrange
    const content = `
    import { Type1 as Type2, Type3 } from './someDir';
`;
    const expected: ParsedImport[] = [
      { type: "Type1", alias: "Type2", importPath: "./someDir" },
      { type: "Type3", alias: "Type3", importPath: "./someDir" }
    ];

    // act
    const result = parseFile(file(content));

    // assert
    checkImports(expect, result.imports, expected);
  });

  it("should parse multiline imports", expect => {
    // arrange
    const content = `
    import { Type1 as Type2,
       Type3 } from './someDir';
    import {
      Type4 as Type5,
      Type6 } from './someDir';
    import {
        Type7 as Type8,
        Type9
    } from './someDir';
`;
    const expected: ParsedImport[] = [
      { type: "Type1", alias: "Type2", importPath: "./someDir" },
      { type: "Type3", alias: "Type3", importPath: "./someDir" },
      { type: "Type4", alias: "Type5", importPath: "./someDir" },
      { type: "Type6", alias: "Type6", importPath: "./someDir" },
      { type: "Type7", alias: "Type8", importPath: "./someDir" },
      { type: "Type9", alias: "Type9", importPath: "./someDir" }
    ];

    // act
    const result = parseFile(file(content));

    // assert
    checkImports(expect, result.imports, expected);
  });

});

function checkImports(expect: IExpect, imports: ParsedImport[], expected: ParsedImport[]): void {
  expect.false(imports === undefined);
  expect.equal(imports.length, expected.length);
  for (let exp of expected) {
    const imp = imports.find(i => i.importPath === exp.importPath && i.type === exp.type);
    expect.false(imp === undefined);
    expect.equal(imp.alias, exp.alias);
  }
}
