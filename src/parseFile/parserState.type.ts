import { ParsedModel } from "./types";
import { BaseState } from "./baseState.type";

export interface ParserState extends BaseState {
  comments: string[];
  model: ParsedModel;
}
