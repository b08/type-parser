export interface ParsedField {
  comments: string[];
  fieldName: string;
  isOptional: boolean;
  typeName: string;
}
