import { ParsedField } from "./parsedField.type";
import { ParsedFunction } from "./parsedFunction.type";
import { ParsedConstructor } from "./parsedConstructor.type";

export interface ParsedClass {
  comments: string[];
  name: string;
  extends: string[];
  implements: string[];
  constructor: ParsedConstructor;
  fields: ParsedField[];
  methods: ParsedFunction[];
}
