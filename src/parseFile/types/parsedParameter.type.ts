export interface ParsedParameter {
  parameterName: string;
  isOptional: boolean;
  typeName: string;
}
