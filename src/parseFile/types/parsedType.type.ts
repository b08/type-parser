export interface ParsedType {
  name: string;
  comments: string[];
}
