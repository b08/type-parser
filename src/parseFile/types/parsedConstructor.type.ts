import { ParsedParameter } from "./parsedParameter.type";

export interface ParsedConstructor {
  parameters: ParsedParameter[];
  comments: string[];
}
