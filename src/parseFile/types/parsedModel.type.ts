import { FileToParse } from "../../fileToParse.type";
import { ParsedClass } from "./parsedClass.type";
import { ParsedEnum } from "./parsedEnum.type";
import { ParsedFunction } from "./parsedFunction.type";
import { ParsedImport } from "./parsedImport.type";
import { ParsedInterface } from "./parsedInterface.type";
import { ParsedSynonym } from "./parsedSynonym.type";
import { ParsedType } from "./parsedType.type";

export interface ParsedModel {
  file: FileToParse;
  imports: ParsedImport[];
  constants: ParsedType[];
  types: ParsedType[];
  enums: ParsedEnum[];
  interfaces: ParsedInterface[];
  classes: ParsedClass[];
  functions: ParsedFunction[];
  synonyms: ParsedSynonym[];
}
