export interface BaseState {
  tokens: string[];
  current: number;
  end: number;
}

export function baseState(state: BaseState): BaseState {
  return {
    tokens: state.tokens,
    current: state.current,
    end: state.end
  };
}
