const forbidden = new Set(["{", "${", "(", "\"", "'", "`", "//", "/*",
  "=", ";",
  "}", ")", "]"]);
export function isForbiddenTokenInsideAngleBrace(token: string): boolean {
  return forbidden.has(token);
}
