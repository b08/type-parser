import { isLineFeed } from "../functions/isLineFeed";

const bracketPairs = {
  "{": "}",
  "[": "]",
  "${": "}",
  "<": ">",
  "(": ")",
  "\"": "\"",
  "'": "'",
  "`": "`",
  "/*": "*/"
};

export function isClosing(token: string, opening: string): boolean {
  return bracketPairs[opening] === token || isSingleComment(token, opening);
}

function isSingleComment(token: string, opening: string): boolean {
  return opening === "//" && isLineFeed(token);
}
