import { BaseState } from "../baseState.type";
import { moveToIndex } from "../functions/moveToIndex";
import { token } from "../functions/currentToken";
import { isOpening } from "./isOpening";
import { last, trimOne } from "@b08/array";
import { isClosing } from "./isClosing";
import { isForbiddenTokenInsideAngleBrace } from "./isForbiddenTokenInsideAngleBrace";

export function closeBracket<T extends BaseState>(state: T): T {
  const closing = findClosingBracket(state);
  return moveToIndex(state, closing + 1);
}

export function findClosingBracket(state: BaseState, index: number = state.current, end: number = state.end): number {
  let stack: string[] = [token(state, index)];

  index++;
  while (index < end) {
    const tok = token(state, index);

    if (last(stack) === "<") {
      if (isForbiddenTokenInsideAngleBrace(tok) && stack.length > 1) {
        stack = trimOne(stack);
      }
    }
    if (isOpening(tok, last(stack))) {
      stack = [...stack, tok];
      index++;
      continue;
    }

    if (isClosing(tok, last(stack))) {
      if (stack.length === 1) { return index; }
      stack = trimOne(stack);
    }

    index++;
  }
  return index;
}
