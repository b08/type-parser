
export function isOptional(name: string): boolean {
  return name.endsWith("?");
}

export function clearOptional(name: string): string {
  return isOptional(name)
    ? name.substr(0, name.length - 1)
    : name;
}
