export function isTypeNameCharacter(token: string): boolean {
  return !!token.match(/^[\$_A-Za-z\d]*$/);
}

export function isFieldNameCharacter(token: string): boolean {
  return !!token.match(/^[\$\?_A-Za-z\d\s]*$/);
}
