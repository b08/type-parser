import { BaseState } from "../baseState.type";

export function moveNext<T extends BaseState>(state: T): T {
  return moveToIndex(state, state.current + 1);
}

export function moveToIndex<T extends BaseState>(state: T, index: number): T {
  return { ...state, current: index };
}
