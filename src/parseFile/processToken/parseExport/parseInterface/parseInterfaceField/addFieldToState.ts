import { ParsedField } from "../../../../types";
import { InterfaceState } from "../interfaceState.type";
import { checkStageEnd } from "./findFieldEnd";
import { isOptional, clearOptional } from "../../../../functions/isOptional";

export function addFieldToState<T extends InterfaceState>(state: T, fieldName: string, typeName: string, current: number): T {
  const field: ParsedField = {
    fieldName: clearOptional(fieldName),
    isOptional: isOptional(fieldName),
    typeName,
    comments: state.comments
  };
  return checkStageEnd({
    ...state,
    fields: [...state.fields, field],
    current
  });
}
