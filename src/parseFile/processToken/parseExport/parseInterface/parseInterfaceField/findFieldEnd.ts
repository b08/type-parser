import { token } from "../../../../functions/currentToken";
import { isLineFeed } from "../../../../functions/isLineFeed";
import { isOpening } from "../../../../closingBrace/isOpening";
import { findClosingBracket } from "../../../../closingBrace/findClosingBrace";
import { BaseState } from "../../../../baseState.type";
import { isInFile } from "../../../../functions/isInFile";

export function checkStageEnd<T extends BaseState>(state: T, current: number = state.current): T {
  if (token(state, current) === "}") {
    return {
      ...state,
      end: current
    };
  } else {
    return {
      ...state,
      current: state.current + 1
    };
  }
}

export function isFieldEnd(token: string): boolean {
  return token === "}" || token === ";" || token === "," || isLineFeed(token);
}

export function endField<T extends BaseState>(state: T, current: number = state.current): T {
  return checkStageEnd({
    ...state,
    current: findFieldEnd(state, current)
  });
}

export function findFieldEnd(state: BaseState, current: number = state.current): number {
  while (isInFile(state, current)) {
    const tok = token(state, current);
    if (isFieldEnd(tok)) { return current; }
    if (isOpening(tok)) {
      current = findClosingBracket(state, current);
    }
    current++;
  }
  return current;
}
