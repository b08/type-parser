import { globalConstants } from "../../../../../globalConstants.const";
import { ParsedFunction, ParsedParameter } from "../../../../types";
import { InterfaceState } from "../interfaceState.type";
import { checkStageEnd } from "./findFieldEnd";
import { keywords } from "../../../../tokens.const";

export function addMethodToState<T extends InterfaceState>(state: T, name: string,
  parameters: ParsedParameter[], returnType: string, current: number): T {
  const async = name.startsWith(keywords.async);
  if (async) { name = name.substr(keywords.async.length); }
  if (returnType === "") { returnType = globalConstants.any; }
  const method: ParsedFunction = {
    name,
    async,
    parameters,
    returnType,
    comments: state.comments
  };
  return checkStageEnd({
    ...state,
    methods: [...state.methods, method],
    current
  });
}
