import { token } from "../../../../functions/currentToken";
import { nonSpaceIndex } from "../../../../functions/nonSpaceIndex";
import { parseParameters } from "../../parseParameters/parseParameters";
import { InterfaceState } from "../interfaceState.type";
import { endField } from "./findFieldEnd";
import { parseMethodReturnType } from "./parseInterfaceMethod";

export function parseDirectMethod(state: InterfaceState, name: string, paramsStart: number): InterfaceState {
  const paramState = parseParameters(state, paramsStart);
  const nonSpace = nonSpaceIndex(state, paramState.next);
  const tok = token(state, nonSpace);
  if (tok === ":") {
    return parseMethodReturnType(state, name, paramState.parameters, nonSpace + 1);
  }

  return endField(state, nonSpace);
}
