import { ParserState } from "../../parserState.type";
import { nonEmptyIndex, emptyIndex } from "../../functions/nonSpaceIndex";
import { token } from "../../functions/currentToken";
import { keywords } from "../../tokens.const";
import { isQuote } from "../parseImport/isQuote";
import { moveToIndex } from "../../functions/moveToIndex";
import { nextIndex } from "../../functions/nextIndex";
import { getTrimmedString } from "../../functions/getString";

export function parseSynonym(state: ParserState): ParserState {
  const from = nonEmptyIndex(state, state.current + 1);
  if (token(state, from) !== keywords.from) { return moveToIndex(state, emptyIndex(state, from)); }
  const leftQuote = nonEmptyIndex(state, from + 1);
  if (!isQuote(token(state, leftQuote))) { return moveToIndex(state, emptyIndex(state, leftQuote)); }
  const rightQuote = nextIndex(state, isQuote, leftQuote + 1);
  const importPath = getTrimmedString(state, leftQuote + 1, rightQuote);
  if (!isGoodPath(importPath)) { return moveToIndex(state, emptyIndex(state, rightQuote)); }
  return createSynonym(moveToIndex(state, rightQuote + 1), importPath);
}

function isGoodPath(importPath: string): boolean {
  return importPath.match(/\.\/[^@\s\[\]{}\(\)<>;\/]+/) != null;
}

function createSynonym(state: ParserState, importPath: string): ParserState {
  const synonym = { path: importPath };
  return {
    ...state,
    model: {
      ...state.model,
      synonyms: [...state.model.synonyms, synonym]
    }
  };
}
