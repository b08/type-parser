import { globalConstants } from "../../../../globalConstants.const";
import { BaseState } from "../../../baseState.type";
import { findClosingBracket } from "../../../closingBrace/findClosingBrace";
import { isOpening } from "../../../closingBrace/isOpening";
import { token } from "../../../functions/currentToken";
import { getTrimmedString } from "../../../functions/getString";
import { isInFile } from "../../../functions/isInFile";
import { isFieldNameCharacter } from "../../../functions/isNameCharacter";
import { nextIndex } from "../../../functions/nextIndex";
import { ParsedParameter } from "../../../types";
import { ParametersState } from "./parametersState.type";
import { isOptional, clearOptional } from "../../../functions/isOptional";

export function parseParameters(state: BaseState, current: number): ParametersState {
  let parameters: ParsedParameter[] = [];
  const addParameter = (parameterName: string, typeName: string) => {
    if (typeName === "") { typeName = globalConstants.any; }
    if (parameterName !== "") {
      const parameter = {
        parameterName: clearOptional(parameterName),
        isOptional: isOptional(parameterName),
        typeName
      };
      parameters.push(parameter);
    }
  };

  while (isInFile(state, current)) {
    const nameEnd = nextIndex(state, tok => !isFieldNameCharacter(tok), current);
    const name = getTrimmedString(state, current, nameEnd);
    const tok = token(state, nameEnd);
    if (tok === ")") {
      addParameter(name, globalConstants.any);
      current = nameEnd + 1;
      break;
    }
    if (tok === ",") {
      addParameter(name, globalConstants.any);
      current = nameEnd + 1;
    }
    if (tok === ":") {
      const typeStart = nameEnd + 1;
      const end = findParameterEnd(state, typeStart);
      const typeName = getTrimmedString(state, typeStart, end);
      addParameter(name, typeName);
      current = end + 1;
      if (token(state, end) === ")") { break; }
    }
    current++;
  }

  return {
    parameters,
    next: current
  };
}

export function findParameterEnd(state: BaseState, current: number = state.current): number {
  while (isInFile(state, current)) {
    const tok = token(state, current);
    if (tok === "," || tok === ")") { return current; }
    if (isOpening(tok)) {
      current = findClosingBracket(state, current);
    }
    current++;
  }
  return current;
}


