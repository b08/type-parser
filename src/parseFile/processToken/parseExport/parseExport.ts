import { token } from "../../functions/currentToken";
import { moveToIndex } from "../../functions/moveToIndex";
import { nonSpaceIndex } from "../../functions/nonSpaceIndex";
import { ParserState } from "../../parserState.type";
import { types, keywords } from "../../tokens.const";
import { parseFunction, parseAsyncFunction } from "./parseFunction/parseFunction";
import { parseInterface } from "./parseInterface/parseInterface";
import { parseType, parseConstant } from "./parseType/parseType";
import { parseEnum } from "./parseEnum/parseEnum";
import { parseClass } from "./parseClass/parseClass";
import { parseSynonym } from "./parseSynonym";

export function parseExport(state: ParserState): ParserState {
  state = moveToIndex(state, nonSpaceIndex(state, state.current + 1));
  switch (token(state)) {
    case types.function:
      return parseFunction(state, false);
    case keywords.async:
      return parseAsyncFunction(state);
    case types.interface:
      return parseInterface(state);
    case types.type:
      return parseType(state);
    case types.constant:
      return parseConstant(state);
    case types.enum:
      return parseEnum(state);
    case types.class:
      return parseClass(state);
    case "*":
      return parseSynonym(state);
    default:
      return state;
  }
}

