import { ParsedEnumValue } from "../../../types";
import { StateWithComment } from "../../comments/stateWithComment.type";

export interface EnumState extends StateWithComment {
  values: ParsedEnumValue[];
}
