import { nonSpaceIndex } from "../../../../functions/nonSpaceIndex";
import { parseName } from "../../parseName/parseName";
import { endField } from "../../parseInterface/parseInterfaceField/findFieldEnd";
import { token } from "../../../../functions/currentToken";
import { ClassState } from "../classState.type";
import { parseClassMethod } from "./parseClassMethod";
import { parseClassFieldType } from "./parseClassFieldType";

export function parseClassField(state: ClassState): ClassState {
  const nameState = parseName(state, state.current + 1);
  const afterName = nonSpaceIndex(state, nameState.next);
  switch (token(state, afterName)) {
    case "(":
      return parseClassMethod(state, nameState.name, afterName + 1);
    case ":":
      return parseClassFieldType(state, nameState.name, afterName + 1);
    default:
      return endField(state, afterName);
  }
}
