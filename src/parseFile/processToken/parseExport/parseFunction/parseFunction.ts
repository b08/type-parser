import { findClosingBracket } from "../../../closingBrace/findClosingBrace";
import { isInFile } from "../../../functions/isInFile";
import { tokenIndex } from "../../../functions/nextIndex";
import { ParserState } from "../../../parserState.type";
import { ParsedFunction } from "../../../types";
import { parseTypeName } from "../parseName/parseName";
import { parseParameters } from "../parseParameters/parseParameters";
import { parseReturnType } from "./parseReturnType";
import { nonSpaceIndex } from "../../../functions/nonSpaceIndex";
import { token } from "../../../functions/currentToken";
import { types } from "../../../tokens.const";
import { moveToIndex } from "../../../functions/moveToIndex";

export function parseAsyncFunction(state: ParserState): ParserState {
  const func = nonSpaceIndex(state, state.current + 1);
  if (token(state, func) !== types.function) { return state; }
  return parseFunction(moveToIndex(state, func), true);
}

export function parseFunction(state: ParserState, async: boolean): ParserState {
  const nameState = parseTypeName(state, state.current + 1);
  const leftBrace = tokenIndex(state, "(", nameState.next);
  if (!isInFile(state, leftBrace)) { return state; }

  const parametersState = parseParameters(state, leftBrace + 1);
  const afterParams = parametersState.next;
  if (!isInFile(state)) { return state; }

  const curlyBrace = tokenIndex(state, "{", afterParams);
  if (!isInFile(state, curlyBrace)) { return state; }

  const closingBrace = findClosingBracket(state, curlyBrace);
  const name = nameState.name;
  const returnType = parseReturnType(state, afterParams, curlyBrace);
  if (name == null || name === "" || returnType == null) { return state; }

  const newFunc = { name, async, parameters: parametersState.parameters, returnType, comments: state.comments };
  return addFunction(state, newFunc, closingBrace);
}

function addFunction(state: ParserState, func: ParsedFunction, closing: number): ParserState {
  return {
    ...state,
    model: {
      ...state.model,
      functions: [...state.model.functions, func]
    },
    current: closing + 1
  };
}
