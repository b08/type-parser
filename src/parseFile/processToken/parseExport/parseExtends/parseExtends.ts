import { isInFile } from "../../../functions/isInFile";
import { ParserState } from "../../../parserState.type";
import { ExtendsState, ExtendsStateId } from "./extendsState.type";
import { parseExtendsToken } from "./parseExtendsToken";

export function parseExtends(state: ParserState, current: number = state.current, end: number = state.end): ExtendsState {
  let extendsState: ExtendsState = {
    tokens: state.tokens,
    end,
    current,
    typeStart: current,
    extendsTypes: [],
    implementsTypes: [],
    stateId: ExtendsStateId.none
  };

  while (isInFile(extendsState)) {
    extendsState = parseExtendsToken(extendsState);
  }

  return extendsState;
}


