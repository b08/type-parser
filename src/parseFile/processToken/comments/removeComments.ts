import { StateWithComment } from "./stateWithComment.type";

export function removeComments<T extends StateWithComment>(state: T): T {
  return { ...state, comments: [] };
}
