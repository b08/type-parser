import { flatMap } from "@b08/array";

export function splitComment(comment: string): string[] {
  return comment.split("|").map(s => s.trim()).filter(x => x.length > 0);
}

export function splitMultilineComment(comment: string): string[] {
  return flatMap(comment.split(/[\r\n]+/), splitComment);
}
