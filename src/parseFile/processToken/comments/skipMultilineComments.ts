import { getTrimmedString } from "../../functions/getString";
import { tokenIndex } from "../../functions/nextIndex";
import { StateWithComment } from "./stateWithComment.type";
import { splitMultilineComment } from "./splitComment";

export function skipMultiLineComment<T extends StateWithComment>(state: T): T {
  const closing = tokenIndex(state, "*/");
  const comment = getTrimmedString(state, state.current + 1, closing);

  return {
    ...state,
    comments: [...state.comments, ...splitMultilineComment(comment)],
    current: closing + 1
  };
}
