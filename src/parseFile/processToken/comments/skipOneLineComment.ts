import { getTrimmedString } from "../../functions/getString";
import { lineFeedIndex } from "../../functions/isLineFeed";
import { StateWithComment } from "./stateWithComment.type";
import { splitComment } from "./splitComment";

export function skipOneLineComment<T extends StateWithComment>(state: T): T {
  const linefeed = lineFeedIndex(state);
  const comments = getTrimmedString(state, state.current + 1, linefeed);

  return {
    ...state,
    comments: [...state.comments, ...splitComment(comments)],
    current: linefeed + 1
  };
}
