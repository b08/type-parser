export function isGoodPath(path: string): boolean {
  return path.match(/^@?[^@\s\[\]{}\(\)<>;]+$/) != null;
}
