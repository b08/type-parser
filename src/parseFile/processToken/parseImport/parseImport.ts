import { token } from "../../functions/currentToken";

import { tokenIndex, nextIndex } from "../../functions/nextIndex";
import { emptyIndex, nonEmptyIndex } from "../../functions/nonSpaceIndex";
import { ParserState } from "../../parserState.type";
import { keywords } from "../../tokens.const";
import { getString, getTrimmedString } from "../../functions/getString";
import { moveNext, moveToIndex } from "../../functions/moveToIndex";
import { createImports } from "./createImports";
import { isQuote } from "./isQuote";
import { isGoodPath } from "./isGoodPath";

export function parseImport(state: ParserState): ParserState {
  state = moveNext(state);
  const leftBracket = nonEmptyIndex(state);
  if (token(state, leftBracket) !== "{") { return moveToIndex(state, emptyIndex(state, leftBracket)); }
  const rightBracket = tokenIndex(state, "}");
  const from = nonEmptyIndex(state, rightBracket + 1);
  if (token(state, from) !== keywords.from) { return moveToIndex(state, emptyIndex(state, from)); }
  const leftQuote = nonEmptyIndex(state, from + 1);
  if (!isQuote(token(state, leftQuote))) { return moveToIndex(state, emptyIndex(state, leftQuote)); }
  const rightQuote = nextIndex(state, isQuote, leftQuote + 1);
  const imports = getString(state, leftBracket + 1, rightBracket);
  const importPath = getTrimmedString(state, leftQuote + 1, rightQuote);
  if (!isGoodPath(importPath)) { return moveToIndex(state, emptyIndex(state, rightQuote)); }
  return createImports(moveToIndex(state, rightQuote + 1), imports, importPath);
}
