export function isQuote(token: string): boolean {
  return token === "'" || token === "\"";
}
