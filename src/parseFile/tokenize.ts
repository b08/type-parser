import { tokenizePlain } from "@b08/tokenize";
import { tokens } from "./tokens.const";

export function tokenize(content: string): string[] {
  return tokenizePlain(content, tokens);
}
