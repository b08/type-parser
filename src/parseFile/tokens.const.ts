
export const types = {
  function: "function ",
  class: "class ",
  interface: "interface ",
  type: "type ",
  enum: "enum ",
  constant: "constant "
};

export const brackets = [
  "(", ")", "[", "]", "{", "}", "<", ">", "'", "\"", "`", "${"
];

export const keywords = {
  export: "export ",
  import: "import",
  from: "from",
  async: "async ",
  extends: "extends ",
  implements: "implements ",
  constructor: "constructor",
  public: "public "
};

export const comments = ["//", "/*", "*/"];
const restOfTokens = [
  " ", "\n", "\r\n", "\r", ",", ";", ":", "\t", "=>", "=", "?", "*"
];

export const tokens = [
  keywords.export, keywords.import, keywords.from, keywords.extends, keywords.implements,
  keywords.constructor, keywords.public, keywords.async,
  types.class, types.function, types.enum, types.type, types.interface, types.constant,
  ...brackets,
  ...comments,
  ...restOfTokens
];
