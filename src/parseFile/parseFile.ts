import { ParserState } from "./parserState.type";
import { tokenize } from "./tokenize";
import { ParsedModel } from "./types";
import { processToken } from "./processToken/processToken";
import { isInFile } from "./functions/isInFile";
import { FileToParse } from "../fileToParse.type";

export function parseFile(file: FileToParse): ParsedModel {
  const model: ParsedModel = {
    file: null,
    imports: [],
    enums: [],
    classes: [],
    interfaces: [],
    functions: [],
    types: [],
    constants: [],
    synonyms: []
  };
  const tokens = tokenize(file.contents);
  let state: ParserState = {
    model,
    tokens,
    current: 0,
    end: tokens.length,
    comments: []
  };
  while (isInFile(state)) {
    state = processToken(state);
  }

  return state.model;
}


