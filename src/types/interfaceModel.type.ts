import { FieldModel } from "./field.model";
import { MethodModel } from "./function.model";
import { TypeModel } from "./type.model";
import { TypeDescription } from "./typeDescription.type";

export interface InterfaceModel extends TypeModel {
  fields: FieldModel[];
  methods: MethodModel[]; // will be null in 1st release
  extends: TypeDescription[];
}
