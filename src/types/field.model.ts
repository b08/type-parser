import { TypeDescription } from "./typeDescription.type";

export interface FieldModel {
  fieldName: string;
  isOptional: boolean;
  comments: string[];
  fieldType: TypeDescription;
}
