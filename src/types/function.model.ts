import { ParameterModel } from "./parameter.model";
import { TypeModel } from "./type.model";
import { TypeDescription } from "./typeDescription.type";

export interface FunctionModel extends TypeModel {
  returnType: TypeDescription;
  parameters: ParameterModel[];
  async: boolean;
}

export interface MethodModel {
  comments: string[];
  name: string;
  async: boolean;
  returnType: TypeDescription;
  parameters: ParameterModel[];
}

export interface ConstructorModel {
  comments: string[];
  parameters: ParameterModel[];
}
