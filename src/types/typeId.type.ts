export interface TypeId {
  // full path, including file name
  path: string;
  // entity(class, function, etc) name
  name: string;
}

export interface ImportedType {
  alias: string;
  // need to save original import path, in case entity is imported via node_modules import, like "@b08/array"
  // if null, then it was not a node import.
  nodeModulesImport: string;

  // if id is null, it means the type was neither imported, nor defined in the same file.
  // happens for type like void, string and etc.
  id: TypeId;
}
