import { ImportedType } from "./typeId.type";

// used to fully describe a type of parameter, return type etc.
export interface TypeDescription {
  typeName: string;
  type: ImportedType;
  genericArgs: TypeDescription[];
}
