import { TypeDescription } from "./typeDescription.type";

export interface ParameterModel {
  parameterName: string;
  isOptional: boolean;
  parameterType: TypeDescription;
}
