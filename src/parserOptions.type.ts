export interface ParserOptions {
  nodeModulesPath?: string;
  defaultModulePath?: string;
}
