import { ParsedModel } from "../parseFile/types";

export function getNodeModulesPath(all: ParsedModel[]): string {
  const paths = all
    .map(m => getNodeModulesPathFromModel(m))
    .filter(p => p != null)
    .sort((p1, p2) => p1.length - p2.length);
  return paths?.[0];
}

const nodeModules = "node_modules";

function getNodeModulesPathFromModel(model: ParsedModel): string {
  const index = model.file.folder.indexOf(nodeModules);
  if (index === -1) { return null; }
  return model.file.folder.slice(0, index + nodeModules.length);
}
