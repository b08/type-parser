import { KeyMap } from "@b08/flat-key";
import { ParserOptions } from "../parserOptions.type";
import { TypeId } from "../types";

export interface ExportOptions extends ParserOptions {
  translations: TypeTranslations;
}

export type TypeTranslations = KeyMap<TypeId, TypeId>;
