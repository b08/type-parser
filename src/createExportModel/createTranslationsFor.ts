import { flatMap } from "@b08/array";
import { FileToParse } from "../fileToParse.type";
import { combinePath } from "../functions/combinePath";
import { ParserOptions } from "../parserOptions.type";
import { TypeId } from "../types";
import { createTypeId } from "./models/importedTypes/createTypeId";

export interface Translation {
  from: TypeId;
  to: TypeId;
}

export function createTranslationsFor(file: FileToParse, type: string,
  backTracks: Map<string, FileToParse[]>, options: ParserOptions): Translation[] {
  return createTranslationsForRef(file, file, type, backTracks, options);
}

function createTranslationsForRef(refFile: FileToParse, thisFile: FileToParse, type: string,
  backTracks: Map<string, FileToParse[]>, options: ParserOptions): Translation[] {
  let filesReferencingThisFile = backTracks.get(combinePath(refFile.folder, refFile.name)) || [];
  if (refFile.name === "index") {
    const indexBacktracks = backTracks.get(refFile.folder);
    filesReferencingThisFile = [...filesReferencingThisFile, ...indexBacktracks || []];
  }

  return [
    ...flatMap(filesReferencingThisFile, f => createTranslationsForFile(f, thisFile, type, options)),
    ...flatMap(filesReferencingThisFile, f => createTranslationsForRef(f, thisFile, type, backTracks, options))
  ];
}

function createTranslationsForFile(refFile: FileToParse, thisFile: FileToParse, type: string, options: ParserOptions): Translation[] {
  const from = createTypeId(type, refFile);
  const to = createTypeId(type, thisFile);
  const result: Translation[] = [{ from, to }];

  const module = tryGetNodeModuleByModuleIndex(refFile, options);
  if (module != null) {
    const fromModule: TypeId = {
      path: module,
      name: type
    };
    result.push({ from: fromModule, to });
  }

  return result;
}

function tryGetNodeModuleByModuleIndex(file: FileToParse, options: ParserOptions): string {
  if (file.name !== "index") { return null; }
  const firstPart = `^${options.nodeModulesPath}\/([^\/]+)\/([^\/]+)`;
  const regex = new RegExp(combinePath(firstPart, options.defaultModulePath) + "$");
  const match = file.folder.match(regex);
  if (match == null) {
    return null;
  }
  return `${match[1]}/${match[2]}`;
}
