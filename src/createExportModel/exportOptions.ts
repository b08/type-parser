import { ParsedModel } from "../parseFile/types";
import { ParserOptions } from "../parserOptions.type";
import { createTypeTranslations } from "./createTypeTranslations";
import { getNodeModulesPath } from "./getNodeModulesPath";
import { ExportOptions } from "./pathTranslation.type";

const defaultOptions: ParserOptions = {
  defaultModulePath: "src"
};

export function exportOptions(all: ParsedModel[], options: ParserOptions): ExportOptions {
  options = {
    defaultModulePath: options.defaultModulePath ?? defaultOptions.defaultModulePath,
    nodeModulesPath: options.nodeModulesPath ?? getNodeModulesPath(all)
  };
  const result: ExportOptions = {
    ...options,
    translations: createTypeTranslations(all, options)
  };
  return result;
}
