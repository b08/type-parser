import { FileToParse } from "../fileToParse.type";
import { ParsedModel } from "../parseFile/types";
import { TypesModel } from "../types";
import { createClassModel } from "./models/createClassModel";
import { createEnumModel } from "./models/createEnumModel";
import { createFunctionModel } from "./models/createFunctionModel";
import { createInterfaceModel } from "./models/createInterfaceModel";
import { createTypeModel } from "./models/createTypeModel";
import { ExportOptions } from "./pathTranslation.type";

export function createExportModel<T extends FileToParse>(src: ParsedModel, all: ParsedModel[], options: ExportOptions): TypesModel<T> {
  return {
    file: src.file as T,
    constants: src.constants.map(c => createTypeModel(c, src)),
    types: src.types.map(t => createTypeModel(t, src)),
    enums: src.enums.map(e => createEnumModel(e, src)),
    interfaces: src.interfaces.map(i => createInterfaceModel(i, src, options)),
    classes: src.classes.map(c => createClassModel(c, src, options)),
    functions: src.functions.map(f => createFunctionModel(f, src, options))
  };
}
