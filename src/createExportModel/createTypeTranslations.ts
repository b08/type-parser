import { KeyMap } from "@b08/flat-key";
import { FileToParse } from "../fileToParse.type";
import { ParsedModel } from "../parseFile/types";
import { ParserOptions } from "../parserOptions.type";
import { TypeId } from "../types";
import { createTranslationsFor } from "./createTranslationsFor";
import { getAbsolutePath, isNodeModulesPath } from "./models/importedTypes/getAbsolutePath";
import { parsedExportedTypes } from "./models/importedTypes/parsedExportedTypes";
import { TypeTranslations } from "./pathTranslation.type";

export function createTypeTranslations(all: ParsedModel[], options: ParserOptions): TypeTranslations {
  const backTracks = createBackTracks(all);
  const translations = new KeyMap<TypeId, TypeId>();
  for (let model of all) {
    for (let type of parsedExportedTypes(model)) {
      for (let trans of createTranslationsFor(model.file, type, backTracks, options)) {
        translations.set(trans.from, trans.to);
      }
    }
  }
  return translations;
}

function createBackTracks(all: ParsedModel[]): Map<string, FileToParse[]> {
  const backTracks = new Map<string, FileToParse[]>();
  for (let model of all) {
    for (let syn of model.synonyms) {
      if (isNodeModulesPath(syn.path)) { continue; }
      const abs = getAbsolutePath(model, syn.path);
      const existing = backTracks.get(abs);
      if (existing != null) {
        existing.push(model.file);
      } else {
        backTracks.set(abs, [model.file]);
      }
    }
  }

  return backTracks;
}
