import { ClassModel, ConstructorModel } from "../../types";
import { ParsedClass, ParsedModel } from "../../parseFile/types";
import { ParsedConstructor } from "../../parseFile/types/parsedConstructor.type";
import { createMethodModel } from "./parts/createMethodModel";
import { createTypeId } from "./importedTypes/createTypeId";
import { createField } from "./parts/createFieldModel";
import { createParameterModel } from "./parts/createParameterModel";
import { createTypeDescriptions } from "./parts/createTypeDescription";
import { ExportOptions } from "../pathTranslation.type";

export function createClassModel(cls: ParsedClass, parsed: ParsedModel, options: ExportOptions): ClassModel {
  return {
    id: createTypeId(cls.name, parsed.file),
    comments: cls.comments,
    fields: cls.fields.map(f => createField(f, parsed, options)),
    methods: cls.methods.map(m => createMethodModel(m, parsed, options)),
    constructor: createConstructor(cls.constructor, parsed, options),
    extends: createTypeDescriptions(cls.extends, parsed, options),
    implements: createTypeDescriptions(cls.implements, parsed, options)
  };
}

function createConstructor(ctor: ParsedConstructor, parsed: ParsedModel, options: ExportOptions): ConstructorModel {
  return ctor && {
    comments: ctor.comments,
    parameters: ctor.parameters.map(p => createParameterModel(p, parsed, options))
  };
}


