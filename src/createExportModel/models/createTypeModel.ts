import { ParsedModel, ParsedType } from "../../parseFile/types";
import { TypeModel } from "../../types";
import { createTypeId } from "./importedTypes/createTypeId";

export function createTypeModel(type: ParsedType, parsed: ParsedModel): TypeModel {
  return {
    comments: type.comments,
    id: createTypeId(type.name, parsed.file)
  };
}
