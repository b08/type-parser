import { InterfaceModel } from "../../types";
import { ParsedInterface, ParsedModel } from "../../parseFile/types";
import { createTypeId } from "./importedTypes/createTypeId";
import { createField } from "./parts/createFieldModel";
import { createMethodModel } from "./parts/createMethodModel";
import { createTypeDescriptions } from "./parts/createTypeDescription";
import { ExportOptions } from "../pathTranslation.type";

export function createInterfaceModel(iface: ParsedInterface, parsed: ParsedModel, options: ExportOptions): InterfaceModel {
  return {
    id: createTypeId(iface.name, parsed.file),
    comments: iface.comments,
    fields: iface.fields.map(f => createField(f, parsed, options)),
    methods: iface.methods.map(m => createMethodModel(m, parsed, options)),
    extends: createTypeDescriptions(iface.extends, parsed, options)
  };
}
