import { ParsedFunction, ParsedModel } from "../../parseFile/types";
import { FunctionModel } from "../../types";
import { ExportOptions } from "../pathTranslation.type";
import { createTypeId } from "./importedTypes/createTypeId";
import { createParameterModel } from "./parts/createParameterModel";
import { createTypeDescription } from "./parts/createTypeDescription";

export function createFunctionModel(func: ParsedFunction, parsed: ParsedModel, options: ExportOptions): FunctionModel {
  return {
    id: createTypeId(func.name, parsed.file),
    async: func.async,
    comments: func.comments,
    parameters: func.parameters.map(p => createParameterModel(p, parsed, options)),
    returnType: createTypeDescription(func.returnType, parsed, options)
  };
}
