import { ParsedEnum, ParsedModel } from "../../parseFile/types";
import { EnumModel } from "../../types";
import { createTypeId } from "./importedTypes/createTypeId";

export function createEnumModel(en: ParsedEnum, parsed: ParsedModel): EnumModel {
  return {
    id: createTypeId(en.name, parsed.file),
    comments: en.comments,
    values: en.values // maybe will need to map later, so far they are the same
  };
}
