import { memoize } from "@b08/cache";
import { ParsedModel } from "../../../parseFile/types";
import { createAbsolutePath } from "./createAbsolutePath";
import { getSplit } from "./getSplit";

export function isNodeModulesPath(importPath: string): boolean {
  return !importPath.startsWith(".");
}

export function getAbsolutePath(parsed: ParsedModel, importPath: string): string {
  return getAbsoluteFolderPath(parsed.file.folder, importPath);
}

const getAbsoluteFolderPath = memoize(function (fileFolder: string, importPath: string): string {
  if (isNodeModulesPath(importPath)) { return importPath; }
  const split = getSplit(importPath);
  if (split.length === 0) { return importPath; }
  return createAbsolutePath(split, fileFolder);
});

