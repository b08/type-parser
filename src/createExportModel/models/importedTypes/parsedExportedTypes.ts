import { memoize } from "@b08/cache";
import { ParsedModel, ParsedType } from "../../../parseFile/types";

export const parsedExportedTypes = memoize(function (model: ParsedModel): Set<string> {
  const typeName = (type: ParsedType) => type.name;
  return new Set([
    ...model.classes.map(typeName),
    ...model.interfaces.map(typeName),
    ...model.enums.map(typeName),
    ...model.types.map(typeName),
    ...model.functions.map(typeName),
    ...model.constants.map(typeName)
  ]);
});
