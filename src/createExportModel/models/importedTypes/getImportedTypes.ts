import { memoize } from "@b08/cache";
import { ParsedModel } from "../../../parseFile/types";
import { ImportedType } from "../../../types";
import { ExportOptions } from "../../pathTranslation.type";
import { createImportedTypes } from "./createImportedType";

export const getImportedTypes = memoize(function (type: string, parsed: ParsedModel, options: ExportOptions): ImportedType[] {
  var types = type.split(/\[|\]|<|>/).filter(x => x.length > 0);
  return createImportedTypes(types, parsed, options).filter(x => x != null);
});
