export function getSplit(path: string): string[] {
  return path.split(/[\\\/]+/).filter(s => s.length > 0);
}
