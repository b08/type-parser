import { FileToParse } from "../../../fileToParse.type";
import { TypeId } from "../../../types";

export function createTypeId(name: string, file: FileToParse): TypeId {
  const path = file.name === "index"
    ? file.folder
    : file.folder + "/" + file.name;
  return {
    name,
    path
  };
}
