import { combinePath } from "../../../functions/combinePath";

export function createPathFromSplit(split: string[], slashed: boolean): string {
  const folder = combinePath(...split);
  return slashed ? "/" + folder : folder;
}
