import { memoize } from "@b08/cache";
import { KeyMap, mapBy } from "@b08/flat-key";
import { ParsedImport, ParsedModel } from "../../../parseFile/types";
import { ImportedType, TypeId } from "../../../types";
import { ExportOptions } from "../../pathTranslation.type";
import { createTypeId } from "./createTypeId";
import { getAbsolutePath, isNodeModulesPath } from "./getAbsolutePath";
import { parsedExportedTypes } from "./parsedExportedTypes";

export const createImportedType = memoize(function (alias: string, parsed: ParsedModel, options: ExportOptions): ImportedType {
  if (parsedExportedTypes(parsed).has(alias)) {
    const typeId = createTypeId(alias, parsed.file);
    return { id: typeId, alias, nodeModulesImport: null };
  }
  const imp = importsMap(parsed).get(alias);
  if (imp == null) {
    return { alias, nodeModulesImport: null, id: null };
  }

  const path = getAbsolutePath(parsed, imp.importPath);
  const type: TypeId = {
    name: imp.type,
    path
  };
  const translated = options.translations.get(type);

  return {
    alias: imp.alias,
    nodeModulesImport: isNodeModulesPath(imp.importPath) ? imp.importPath : null,
    id: translated || type
  };
});

export function createImportedTypes(aliases: string[], parsed: ParsedModel, options: ExportOptions): ImportedType[] {
  return aliases.map(a => createImportedType(a, parsed, options));
}

const importsMap = memoize(function (parsed: ParsedModel): KeyMap<string, ParsedImport> {
  return mapBy(parsed.imports, imp => imp.alias);
});

