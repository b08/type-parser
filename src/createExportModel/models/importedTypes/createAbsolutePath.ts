import { createPathFromSplit } from "./createPathFromSplit";
import { getSplit } from "./getSplit";
import { trimOne } from "@b08/array";

export function createAbsolutePath(split: string[], fileFolder: string): string {
  const slashed = fileFolder.startsWith("/");
  let splitFolder = getSplit(fileFolder);
  let index = 0;
  while (index < split.length) {
    const token = split[index];
    switch (token) {
      case ".": break;
      case "..":
        if (splitFolder.length > 1) {
          splitFolder = trimOne(splitFolder);
        }
        break;
      default:
        splitFolder = [...splitFolder, token];
    }
    index++;
  }

  return createPathFromSplit(splitFolder, slashed);
}
