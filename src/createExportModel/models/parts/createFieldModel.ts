import { ParsedField, ParsedModel } from "../../../parseFile/types";
import { FieldModel } from "../../../types";
import { ExportOptions } from "../../pathTranslation.type";
import { createTypeDescription } from "./createTypeDescription";

export function createField(field: ParsedField, parsed: ParsedModel, options: ExportOptions): FieldModel {
  return {
    fieldName: field.fieldName,
    isOptional: field.isOptional,
    comments: field.comments,
    fieldType: createTypeDescription(field.typeName, parsed, options)
  };
}
