import { ParsedFunction, ParsedModel } from "../../../parseFile/types";
import { MethodModel } from "../../../types";
import { ExportOptions } from "../../pathTranslation.type";
import { createParameterModel } from "./createParameterModel";
import { createTypeDescription } from "./createTypeDescription";

export function createMethodModel(func: ParsedFunction, parsed: ParsedModel, options: ExportOptions): MethodModel {
  return {
    name: func.name,
    async: func.async,
    comments: func.comments,
    parameters: func.parameters.map(p => createParameterModel(p, parsed, options)),
    returnType: createTypeDescription(func.returnType, parsed, options)
  };
}
