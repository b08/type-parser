import { ParsedModel } from "../../../parseFile/types";
import { TypeDescription } from "../../../types";
import { ExportOptions } from "../../pathTranslation.type";
import { createImportedType } from "../importedTypes/createImportedType";
import { findTypeSeparator } from "./findTypeSeparator";

export function createTypeDescriptions(types: string[], parsed: ParsedModel, options: ExportOptions): TypeDescription[] {
  return types.map(t => createTypeDescription(t, parsed, options));
}

export function createTypeDescription(type: string, parsed: ParsedModel, options: ExportOptions): TypeDescription {
  type = type.trim();
  if (type.startsWith("{") || type.endsWith("}") || type.startsWith("(")) { // so far such types will be just ignored
    return { typeName: type, type: null, genericArgs: [] };
  }

  if (type.match(/\[\s*\]$/)) {
    const generic = createTypeDescription(type.substr(0, type.length - 2), parsed, options);
    return {
      typeName: type,
      type: { alias: "Array", nodeModulesImport: null, id: null },
      genericArgs: [generic]
    };
  }

  if (type.endsWith(">")) {
    const start = type.indexOf("<");
    if (start === -1) { throw new Error(`failed to parse type ${type}`); }
    const generic = createGenericTypes(type.substr(start + 1, type.length - start - 2), parsed, options);
    return createDescription(type, type.substr(0, start), generic, parsed, options);
  }

  return createDescription(type, type, [], parsed, options);
}

function createDescription(fullTypeName: string, type: string, genericArgs: TypeDescription[],
  parsed: ParsedModel, options: ExportOptions): TypeDescription {
  return { typeName: fullTypeName, type: createImportedType(type.trim(), parsed, options), genericArgs };
}

function createGenericTypes(types: string, parsed: ParsedModel, options: ExportOptions): TypeDescription[] {
  const separator = findTypeSeparator(types);
  if (separator === types.length) {
    return [createTypeDescription(types, parsed, options)];
  }
  return [
    createTypeDescription(types.substr(0, separator), parsed, options),
    ...createGenericTypes(types.substr(separator + 1), parsed, options)
  ];
}

