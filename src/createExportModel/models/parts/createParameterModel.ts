import { ParsedModel, ParsedParameter } from "../../../parseFile/types";
import { ParameterModel } from "../../../types";
import { ExportOptions } from "../../pathTranslation.type";
import { createTypeDescription } from "./createTypeDescription";

export function createParameterModel(parameter: ParsedParameter, parsed: ParsedModel, options: ExportOptions): ParameterModel {
  return {
    parameterName: parameter.parameterName,
    isOptional: parameter.isOptional,
    parameterType: createTypeDescription(parameter.typeName, parsed, options)
  };
}
