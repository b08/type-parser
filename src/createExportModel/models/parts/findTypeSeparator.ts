import { last } from "@b08/array";
import { isClosing } from "../../../parseFile/closingBrace/isClosing";
import { isOpening } from "../../../parseFile/closingBrace/isOpening";

export function findTypeSeparator(types: string): number {
  const stack = [];
  let index = 0;
  while (index < types.length) {
    if (types[index] === "," && stack.length === 0) { return index; }
    if (isOpening(types[index], last(stack))) {
      stack.push(types[index]);
    }

    if (stack.length > 0 && isClosing(types[index], last(stack))) {
      stack.pop();
    }

    index++;
  }

  return index;
}
