import { createExportModel } from "./createExportModel/createExportModel";
import { exportOptions } from "./createExportModel/exportOptions";
import { FileToParse } from "./fileToParse.type";
import { parseFile } from "./parseFile/parseFile";
import { ParserOptions } from "./parserOptions.type";
import { TypesModel } from "./types";

export function parseTypesInFiles<T extends FileToParse>(files: T[], options: ParserOptions = {}): TypesModel<T>[] {
  const parsed = files.map(file => parseFile(file));
  const exp = exportOptions(parsed, options);
  return parsed.map(p => createExportModel(p, parsed, exp));
}
